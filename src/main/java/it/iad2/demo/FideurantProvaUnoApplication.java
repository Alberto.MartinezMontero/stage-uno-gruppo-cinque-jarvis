package it.iad2.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FideurantProvaUnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FideurantProvaUnoApplication.class, args);
	}
}
